From: =?UTF-8?q?Sebastian=20Dr=C3=B6ge?= <sebastian@centricular.com>
Date: Fri, 4 Oct 2024 13:15:27 +0300
Subject: wavparse: Fix parsing of acid chunk
Origin: https://gitlab.freedesktop.org/gstreamer/gstreamer/-/commit/8911020ae3da65b224dd1c87de3437a532e9efa4

Simply casting the bytes to a struct can lead to crashes because of unaligned
reads, and is also missing the endianness swapping that is necessary on big
endian architectures.

Part-of: <https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/8054>
---
 .../gst-plugins-good/gst/wavparse/gstwavparse.c      | 12 +++++-------
 1 file changed, 5 insertions(+), 7 deletions(-)

--- a/gst/wavparse/gstwavparse.c
+++ b/gst/wavparse/gstwavparse.c
@@ -1433,8 +1433,7 @@ gst_wavparse_stream_headers (GstWavParse
         break;
       }
       case GST_RIFF_TAG_acid:{
-        const gst_riff_acid *acid = NULL;
-        const guint data_size = sizeof (gst_riff_acid);
+        const guint data_size = 24;
         gfloat tempo;
 
         GST_INFO_OBJECT (wav, "Have acid chunk");
@@ -1448,13 +1447,13 @@ gst_wavparse_stream_headers (GstWavParse
           break;
         }
         if (wav->streaming) {
+          const guint8 *data;
           if (!gst_wavparse_peek_chunk (wav, &tag, &size)) {
             goto exit;
           }
           gst_adapter_flush (wav->adapter, 8);
-          acid = (const gst_riff_acid *) gst_adapter_map (wav->adapter,
-              data_size);
-          tempo = acid->tempo;
+          data = gst_adapter_map (wav->adapter, data_size);
+          tempo = GST_READ_FLOAT_LE (data + 20);
           gst_adapter_unmap (wav->adapter);
         } else {
           GstMapInfo map;
@@ -1465,8 +1464,7 @@ gst_wavparse_stream_headers (GstWavParse
                       &buf)) != GST_FLOW_OK)
             goto header_pull_error;
           gst_buffer_map (buf, &map, GST_MAP_READ);
-          acid = (const gst_riff_acid *) map.data;
-          tempo = acid->tempo;
+          tempo = GST_READ_FLOAT_LE (map.data + 20);
           gst_buffer_unmap (buf, &map);
         }
         /* send data as tags */
